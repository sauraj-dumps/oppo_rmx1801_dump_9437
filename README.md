## aosp_RMX1801-userdebug 12 SP1A.210812.015 eng.androi.20211022.090229 test-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: oppo
- Flavor: aosp_RMX1801-userdebug
- Release Version: 12
- Id: SP1A.210812.015
- Incremental: eng.androi.20211022.090229
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OPPO/RMX1801/RMX1801:10/QKQ1.191014.001/1602573502:user/release-keys
- OTA version: 
- Branch: aosp_RMX1801-userdebug-12-SP1A.210812.015-eng.androi.20211022.090229-test-keys
- Repo: oppo_rmx1801_dump_9437


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
